Upon pulling from git repository, please use 

### npm install
This will install the package required by this app.

After finish installing, use 
### npm run start
As this will start your server.js right away

This server serves the purpose for verification of phone number using API powered by 'https://numverify.com'.
This API would provide a phone number check for 100 times.

The main file included would be app.js, server.js and api/index.js, which contains the start of the app, the server and the router from express.js.