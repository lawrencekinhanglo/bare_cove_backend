
var express = require('express')
var bodyParser = require('body-parser')
var app = express()
var router = require('./api/index')
var cors = require('cors')

app.use(cors())
app.use(express.json())

// extra modules
var colors = require('colors')

// Configuration
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// Api
app.use('/', cors(), router)

          
module.exports = app