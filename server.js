var app = require('./app')
var port = process.env.PORT || 3002

var server = app.listen(port, function() {
    console.log((''))
    console.log((''))
    console.log(('-----------------------------------').red)    
    console.log(('Phone Verification Server listening on port ' + port).green)
    console.log(('-----------------------------------').red)
    console.log((''))
    console.log((''))
    console.log(('Morning Sir').yellow)
})