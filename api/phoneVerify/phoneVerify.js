var axios = require('axios')
var CONFIG = require('../../config.js')

async function phoneVerify(req, res) {
    var submit_time = new Date().toISOString()
 
    let response = await axios.get(`${CONFIG.API_WEB_URL}?access_key=${CONFIG.API_ACCESS_KEY}&number=${req.body.number}&country_code=${req.body.country_code}&format=1`)
        if(response.data){
            response.data.submit_time = submit_time
            res.json({status: true, data: response.data})
        }else{
            res.json({status: false, error: 'FAIL_TO_GET_VERIFICATION'})
        }
    
}

module.exports = {phoneVerify}