require('dotenv').config({path:__dirname+'/.env'})
var express = require('express')
var router = express.Router()
var bodyParser = require('body-parser')
var cors = require('cors')
var axios = require('axios')

var phoneVerify = require('./phoneVerify/phoneVerify')

router.use(cors());
router.get('/backend/v1/', function(req, res) {
    res.json({status: true, message: 'Successfully called Phone Verification API'})
})

// GET Request
router.post('/backend/v1/phone-verification/', phoneVerify.phoneVerify)


module.exports = router